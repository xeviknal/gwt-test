package com.zhilabs.minimaxtimetable.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.widget.client.TextButton;
import com.zhilabs.minimaxtimetable.shared.FieldVerifier;

public class TimeLineView {
	private static final int REFRESH_INTERVAL = 1000;
	private static final int VALUES_TO_SHOW   = 50;
	
	private HorizontalPanel mainPanel;
	private VerticalPanel formPanel;
	private VerticalPanel chartPanel;
	private TextBox maxValueTextBox;
	private TextBox minValueTextBox;
	private TextButton submitButton;
	private Label lastUpdatedLabel;
	private Label maxLabel;
	private Label minLabel;
	private HistoryLineChart chart;
	private ArrayList<Integer> history;
	private Timer refreshTimer;
	
	public TimeLineView() {
		mainPanel		= new HorizontalPanel();
		formPanel		= new VerticalPanel();
		chartPanel 		= new VerticalPanel();
		maxValueTextBox = new TextBox();
		minValueTextBox = new TextBox();
		submitButton 	= new TextButton("Submit");
		lastUpdatedLabel= new Label();
		maxLabel		= new Label("Max value:");
		minLabel		= new Label("Min value:");
		history			= new ArrayList<Integer>();
	}
	
	public HorizontalPanel initialize() {
		maxValueTextBox.setText(Cookies.getCookie("maxValue"));
		minValueTextBox.setText(Cookies.getCookie("minValue"));
		
		// Building mini max form
		formPanel.addStyleName("formPanel border");
		formPanel.add(maxLabel);
		formPanel.add(maxValueTextBox);
		formPanel.add(minLabel);
		formPanel.add(minValueTextBox);
		formPanel.add(submitButton);
		
		// Bulding chart + last update date
		chartPanel.addStyleName("chartPanel border");
		chartPanel.setWidth("1024px");
		chartPanel.add(lastUpdatedLabel);
		
		// Building main panel
		mainPanel.add(formPanel);
		mainPanel.add(chartPanel);
		
		chart = new HistoryLineChart(history, chartPanel);
		
		submitButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				setRange();
			}
		});
		
		minValueTextBox.addKeyDownHandler(new KeyDownHandler() {
			public void onKeyDown(KeyDownEvent event) {
				if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER)
					setRange();
			}
		});
		
		return mainPanel;
	}
	
	private void setRange() {
		final int minValue = Integer.parseInt(minValueTextBox.getText());
		final int maxValue = Integer.parseInt(maxValueTextBox.getText());
		
		if(!FieldVerifier.isValidRange(minValue, maxValue)) {
			Window.alert("Range not valid. Please check the values");
			return;
		}
		
		Cookies.setCookie("minValue", minValueTextBox.getText());
		Cookies.setCookie("maxValue", maxValueTextBox.getText());
		
		initTimer();
		refreshChart();
	}

	private void refreshChart(){
		//Generate new value
		final int minValue = Integer.parseInt(minValueTextBox.getText());
		final int maxValue = Integer.parseInt(maxValueTextBox.getText());
		
		history.add(RandomInt.randInt(minValue, maxValue));
		if(history.size() > VALUES_TO_SHOW)
			history.remove(0);
		
		// Last updated
		// Display timestamp showing last refresh.
		DateTimeFormat dateFormat = DateTimeFormat.getFormat(
				DateTimeFormat.PredefinedFormat.DATE_TIME_MEDIUM);
		lastUpdatedLabel.setText("Last update: " 
	        + dateFormat.format(new Date()));
		
		chart.draw();
	}
	
	private void initTimer(){
		if(refreshTimer == null) {
			refreshTimer = new Timer() {
				public void run() {
					refreshChart();
				}
			};

			refreshTimer.scheduleRepeating(REFRESH_INTERVAL);
		}
	}
}
