package com.zhilabs.minimaxtimetable.client;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.CellPanel;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.LineChart;
import com.googlecode.gwt.charts.client.corechart.LineChartOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.VAxis;

public class HistoryLineChart {
	private LineChart chart;
	private ArrayList<Integer> history;
	private CellPanel panel;

	public HistoryLineChart(ArrayList<Integer> history, CellPanel panel) {
		this.history = history;
		this.panel   = panel;
		initialize();
	}

	private void initialize() {
		ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
		chartLoader.loadApi(new Runnable() {

			@Override
			public void run() {
				// Create and attach the chart
				chart = new LineChart();
				panel.add(chart);
				draw();
			}
		});
	}

	public void draw() {
		// Prepare the data
		DataTable dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.NUMBER, "TIME");
		dataTable.addColumn(ColumnType.NUMBER, "VALUE");
		
		dataTable.addRows(history.size());
		for(int i = 0; i < history.size(); i++) {
			dataTable.setValue(i, 0, i);
			dataTable.setValue(i, 1, history.get(i));
		}
		// Set options
		LineChartOptions options = LineChartOptions.create();
		options.setBackgroundColor("#f0f0f0");
		options.setFontName("Tahoma");
		options.setTitle("Evolution of a discrete variable");
		options.setHAxis(HAxis.create("Time"));
		options.setVAxis(VAxis.create("Value"));

		// Draw the chart
		chart.draw(dataTable, options);
	}
}