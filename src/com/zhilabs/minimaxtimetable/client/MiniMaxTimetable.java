package com.zhilabs.minimaxtimetable.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

public class MiniMaxTimetable implements EntryPoint {

	public void onModuleLoad() {
		RootPanel.get("minimax").add(new TimeLineView().initialize());
	}
}
